<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomFieldsToTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('tracks', function(Blueprint $table)
		{
                    $table->string('jilID')->nullable();
                    $table->string('djezzy')->nullable();
                    $table->string('mobilis')->nullable();
                    $table->string('ooredoo')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tracks', function(Blueprint $table)
		{
                    $table->dropColumn('jilID');
                    $table->dropColumn('djezzy');
                    $table->dropColumn('mobilis');
                    $table->dropColumn('ooredoo');
		});
    }
}
