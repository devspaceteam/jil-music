<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdsLimitToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if (Schema::hasColumn('users', 'ads_limit'))
			return;

		Schema::table('users', function (Blueprint $table) {
			$table->integer('ads_limit')->nullable()->default(0);
			$table->timestamp('ads_limit_date')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('ads_limit');
			$table->dropColumn('ads_limit_date');
		});
	}

}
