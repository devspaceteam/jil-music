<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalToAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::table('albums', function (Blueprint $table) {
            $table->string('UPC')->nullable();
            $table->string('distribution_comp')->nullable();
            $table->string('licence_comp')->nullable();
            $table->string('label_year')->nullable();
            $table->string('label_label')->nullable();
            $table->boolean('is_premium')->default(0);            
            $table->boolean('is_live')->default(0);
            $table->boolean('is_mix')->default(0);
            $table->boolean('is_comp')->default(0);
            $table->string('set_state')->default('pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->dropColumn('UPC');
            $table->dropColumn('distribution_comp');
            $table->dropColumn('licence_comp');
            $table->dropColumn('label_year');
            $table->dropColumn('label_label');
            $table->dropColumn('is_premium');
            $table->dropColumn('is_live');
            $table->dropColumn('is_mix');
            $table->dropColumn('is_comp');
            $table->dropColumn('set_state');
        });
    }
}
