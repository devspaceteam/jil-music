<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesToTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_to_track', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('track_id')->unsigned();
                        $table->string('name');
                        $table->string('roles');
                        $table->timestamps();
			
                        $table->collation = config('database.connections.mysql.collation');
                        $table->charset = config('database.connections.mysql.charset');
                        
                        $table->foreign('track_id')
                            ->references('id')->on('tracks')
                            ->onDelete('cascade');
                        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles_to_track');
    }
}
