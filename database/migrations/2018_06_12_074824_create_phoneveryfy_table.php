<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneveryfyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	if (Schema::hasTable('phoneveryfy')) return;

	Schema::create('phoneveryfy', function(Blueprint $table)
	{
            $table->increments('id');
            $table->string('phone')->nullable()->unique();
            $table->string('sms_code', 60)->nullable();
            $table->timestamps();
	});
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phoneveryfy');
    }
}
