<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_ads', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('url')->nullable();
			$table->string('status')->nullable();
			$table->integer('duration')->unsigned()->nullable();
		});
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('video_ads');
    }
}
