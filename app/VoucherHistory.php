<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherHistory extends Model {

    protected $table = 'voucher_activation_history';
	/**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'       => 'integer',
		'user_id'  => 'integer',
        'amount' => 'decimal',
		'voucher'  => 'string',
        'date_sold'   => 'timestamp',
    ];
	
	/**
     * Many to one relationship with album model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {                
        return $this->hasOne('App\User','id','user_id');       
    }

}

