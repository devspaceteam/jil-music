<?php namespace App\Services\Search;

use App\RolesToTrack;

class RolesToTrackSearch {


    /**
     * Search users in local database.
     *
     * @param string  $q
     * @param int     $limit
     * @param string  $type
     *
     * @return array
     */
    public function searchNames($q, $limit = 10)
    {
        $artists_names = RolesToTrack::where('name', 'like', $q.'%')                     
                    ->select('*')
                    ->distinct('name')
                    ->limit($limit)
                    ->get();

        return $artists_names->toArray();
    }
}