<?php namespace App\Services\Providers\Local;

use App\Track;
use Illuminate\Database\Eloquent\Collection;

class LocalTopTracks {

    /**
     * Get top tracks using local provider.
     *
     * @return Collection
     */
    public function getTopTracks() {
        return Track::with('album.artist')
                ->whereIn('tracks.album_id', function($query){
                        $query->select('id')
                        ->from(with(new Album)->getTable())                        
                        ->where('set_state', 'validate');
                })
                ->orderBy('plays', 'desc')->limit(50)->get();
    }
}