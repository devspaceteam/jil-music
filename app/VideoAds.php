<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoAds extends Model {

    protected $table = 'video_ads';
	public $timestamps = false;
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'       => 'integer',
        'name' => 'string',
        'url'   => 'string',
        'status' => 'string',
		'duration' => 'integer',
    ];

}
