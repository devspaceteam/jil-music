<?php namespace App\Http\Controllers;

use App\RolesToTrack;
use Illuminate\Http\Request;
use Vebto\Bootstrap\Controller;

class RolesToTrackController extends Controller {

	
    /**
    * @var RolesToTrack
    */
    private $rolestotrack;
    
    /**
     * @var Request
     */
    private $request;

    /**
     * TrackController constructor.
     *
     * @param RolesToTrack $rolestotrack
     * @param Request $request
     */
    public function __construct(Request $request, RolesToTrack $rolestotrack)
    {		
                $this->request = $request;
                $this->rolestotrack = $rolestotrack;
    }

	
        /**
         * Update existing tracks or create/delete.
         *
         * @param Request $request        
         * @return RolesToTrack
         */
	public function updateRows(Request $request)
	{
		$track_rows = $this->getRolesToTrackByTrackID($this->request->get('id'));   
                
                $track_rows_request = $this->request->get('rolestotrack');
                                
                if(empty($track_rows_request) || !is_array($track_rows_request)){
                    if($track_rows){
                        foreach($track_rows as $key=>$value){
                            $this->deleteRolesToTrackByTrackID($this->request->get('id'));
                        }
                    }                    
                }elseif($track_rows){
                    foreach($track_rows as $key_stored=>$stored_track){                        
                        $need_unset = false;
                        foreach($track_rows_request as $key_new=>$new_track){
                            if($stored_track->id == $new_track['id']){
                                if($stored_track->name != $new_track['name'] || $stored_track->roles != $new_track['roles'] ){
                                    $this->update($stored_track->id, $new_track);   
                                }                            
                                unset($track_rows_request[$key_new]);
                                $need_unset = true;
                            }                                                  
                        }
                        if($need_unset){
                            unset($track_rows[$key_stored]);
                        }                        
                    }
                    
                    if($track_rows != []){
                        foreach($track_rows as $key=>$stored_track){
                            $this->destroy($stored_track['id']);
                        }
                    }
                    if($track_rows_request != []){
                        foreach($track_rows_request as $new_track){
                            $this->store($new_track);
                        }
                    }
                    
                }else{
                    foreach ($track_rows_request as $new_track){
                        $this->store($new_track);
                    }                     
                }
                
                $track_rows = $this->getRolesToTrackByTrackID($this->request->get('id'));   
                
		return $track_rows;
	}
        
        /**
         * Get existing rolesToTrack by track ID.
         *
         * @param int $id_track         
         * @return RolesToTrack
         */
	public function getRolesToTrackByTrackID($id_track)
	{
		$track_rows = $this->rolestotrack->where('track_id',$id_track)->get();
                
		return $track_rows;
	}
        
        /**
        * Update existing rolesToTrack.
        *
        * @param int $id_track
        * @param RolesToTrack $new_track
        * @return RolesToTrack
        */
	public function update($id, $new_track)
	{
		$stored_track = $this->rolestotrack->findOrFail($id);
                
                if(isset($new_track['id'])){
                    unset($new_track['id']);
                }

		$stored_track->fill($new_track)->save();                
                
		return $stored_track;
	}
        
        /**
        * Create a new rolesToTrack.
        *
        * @return RolesToTrack
        */
        public function store($new_track)
        {
            if(isset($new_track['id'])){
                unset($new_track['id']);
            }

            $track_row = $this->rolestotrack->create($new_track);

            return $track_row;
        }      
       
       /**
	 * Remove rolesToTrack from database.
	 *
	 * @return mixed
	 */
	public function destroy($id)
	{
            
	    return $this->rolestotrack->destroy($id);
	}

}
