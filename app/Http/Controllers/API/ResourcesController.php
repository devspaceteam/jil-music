<?php

namespace App\Http\Controllers\API;

use Vebto\Bootstrap\Controller;
use Illuminate\Http\Request;

use App\User;

class ResourcesController extends Controller {
        
    public function getUserSubscription(Request $request ) {
	        
        if(!$request->get('user_id'))        
            return response()->json('Invalid request format', 422);
                
        $user = User::where('id',$request->get('user_id'))->first();
        
        if(!$user)
            return response()->json('No User data found', 422);
        
        $response['is_active'] = $user->subscription_s;
        $response['ends_with'] = $user->subscription;
                
        return response()->json($response);

    }
    
}
