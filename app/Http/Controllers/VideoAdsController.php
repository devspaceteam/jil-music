<?php namespace App\Http\Controllers;

use App\VideoAds;
use Illuminate\Http\Request;
use Vebto\Bootstrap\Controller;
use App\Services\Paginator;
use App\Http\Requests\ModifyVideoAds;

class VideoAdsController extends Controller {

	
    /**
    * @var VideoAds
    */
    private $videoads;
    
    /**
     * @var Request
     */
    private $request;

    /**
     * TrackController constructor.
     *
     * @param RolesToTrack $rolestotrack
     * @param Request $request
     */
    public function __construct(VideoAds $videoads, Request $request)
    {		
                $this->request = $request;
                $this->videoads = $videoads;
    }
	
	public function index()
	{
//        $this->authorize('index', VideoAds::class);

        $params = $this->request->all();
        $params['order_by'] = isset($params['order_by']) ? 'id' : 'id';

	    return (new Paginator($this->videoads))->paginate($params);
	}
	
	public function active()
	{
            return VideoAds::where('status','active')->get();
	}

	/**
	 * Find track matching given id.
	 *
	 * @param  int  $id
	 * @return Track
	 */
	public function show($id)
	{
        $track = $this->videoads->findOrFail($id);

//	    $this->authorize('show', $track);

	    return $track;
	}

    /**
     * Update existing track.
     *
     * @param int $id
     * @param ModifyTracks $validate
     * @return Track
     */
	public function update($id, ModifyVideoAds $validate)
	{
		$track = $this->videoads->findOrFail($id);

//		$this->authorize('update', $track);

		$track->fill($this->request->save());
                
		return $track;
	}

    /**
     * Create a new track.
     *
     * @param ModifyTracks $validate
     * @return Track
     */
    public function store(ModifyVideoAds $validate)
    {
//        $this->authorize('store', Track::class);

        $track = $this->videoads->create($this->request->all());

        return $track;
    }

	/**
	 * Remove tracks from database.
	 *
	 * @return mixed
	 */
	public function destroy()
	{
//		$this->authorize('destroy', Track::class);

        $this->validate($this->request, [
            'ids'   => 'required|array',
            'ids.*' => 'required|integer'
        ]);

	    return $this->videoads->destroy($this->request->get('ids'));
	}
	
	public function activate()
	{
	        $this->validate($this->request, [
                'ids'   => 'required|array',
                'ids.*' => 'required|integer'
            ]);

            VideoAds::whereIn('id', $this->request->get('ids'))->update(['status'=>'active']);
	    
	    return $this->success();
	}
	
	public function disable()
	{
	    $this->validate($this->request, [
                'ids'   => 'required|array',
                'ids.*' => 'required|integer'
            ]);

            VideoAds::whereIn('id', $this->request->get('ids'))->update(['status'=>'disabled']);
	    
	    return $this->success();
	}
}
