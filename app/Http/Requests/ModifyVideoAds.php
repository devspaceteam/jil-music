<?php namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Database\Query\Builder;
use Vebto\Base\BaseFormRequest;

class ModifyVideoAds extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $trackId = $this->route('id');
        
        $rules = [
            'name' => 'required|string|min:1|max:255',
            'duration' => 'required|integer|min:1',
            'status' => 'required|string|min:1|max:255',
        ];

        return $rules;
    }
}
