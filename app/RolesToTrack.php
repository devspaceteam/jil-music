<?php namespace App;

use App\Traits\OrdersByPopularity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\RolesToTrack
 * 
 * @mixin \Eloquent
 */
class RolesToTrack extends Model {
    

    protected $table = 'roles_to_track';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'       => 'integer',
        'track_id' => 'integer',
    ];

    /**
     * Convert artists from string to array. *|* is a delimiter.
     *
     * @param string $artists
     * @return array
     */
    public function getRolesAttribute($roles)
    {
        return explode('*|*', $roles);
    }        
    
}
